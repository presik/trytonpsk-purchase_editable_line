# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import purchase


def register():
    Pool.register(
        purchase.AddProductForm,
        module='purchase_editable_line', type_='model')
    Pool.register(
        purchase.WizardAddProduct,
        module='purchase_editable_line', type_='wizard')
