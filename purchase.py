# This file is part of purchase_editable_line module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool


class AddProductForm(ModelView):
    'Add Product Form'
    __name__ = 'purchase_editable_line.add_product_form'
    purchase = fields.Many2One('purchase.purchase', 'Purchase', required=True)
    lines = fields.One2Many('purchase.line', None, 'Lines',
        context={
            'purchase': Eval('purchase'),
        }, domain=[
            ('purchase', '=', Eval('purchase')),
        ],
        depends=['purchase'])

    @staticmethod
    def default_purchase():
        return Transaction().context.get('active_id')

    @staticmethod
    def default_purchase_state():
        return 'draft'


class WizardAddProduct(Wizard):
    'Wizard Add Product'
    __name__ = 'purchase.add_product'
    start = StateView('purchase_editable_line.add_product_form',
        'purchase_editable_line.add_product_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add and New', 'add_new_', 'tryton-go-jump', default=True),
            Button('Add', 'add_', 'tryton-ok'),
        ])
    add_new_ = StateTransition()
    add_ = StateTransition()

    def default_start(self, fields):
        return {
            'purchase': Transaction().context.get('active_id'),
        }

    def add_lines(self):
        Purchase = Pool().get('purchase.purchase')
        purchase = Purchase(self.start.purchase)
        if purchase.state != 'draft':
            return

        for line in self.start.lines:
            line.purchase = Transaction().context.get('active_id', False)
            line.save()

    def transition_add_new_(self):
        self.add_lines()
        return 'start'

    def transition_add_(self):
        self.add_lines()
        return 'end'
