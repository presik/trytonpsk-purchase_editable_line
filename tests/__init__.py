# This file is part of the purchase_request_editable_list module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from .test_purchase_editable_line import suite

__all__ = ['suite']
